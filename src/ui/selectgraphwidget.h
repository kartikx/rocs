/*
 *  Copyright 2020  Kartik Ramesh <kartikx2000@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License or (at your option) version 3 or any later version
 *  accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 *  defined in Section 14 of version 3 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SELECTGRAPHWIDGET_H
#define SELECTGRAPHWIDGET_H

#include "ui_selectgraphwidget.h"
#include "libgraphtheory/typenames.h"
#include "project/project.h"

#include <QWidget>
#include <QDialog>

class QDialogButtonBox;
class QCheckBox;

class SelectGraphWidget : public QDialog
{
    Q_OBJECT

public:
    SelectGraphWidget(Project* project, QWidget *parent = nullptr);
    ~SelectGraphWidget();

    /*
     * Sets up the Widget Layout
     */
    void setUpWidget();

private Q_SLOTS:
    /**
     * Modifies the visibilty of Graph Documents,
     * based on the inputs given to the CheckBox.
     */
    void modifyVisibleGraphDocumentList();

    /**
     * Allows automatic selection, and
     * deselection of all checkboxes, depending
     * on @p check
     */
    void selectCheckBoxAuto(bool check);

private:
    Ui::SelectGraphWidget *ui;
    Project* m_project;
    QList<GraphTheory::GraphDocumentPtr> m_graphDocuments;
    QList<QCheckBox*> m_checkBoxList;
    QDialogButtonBox* m_buttonBox;
};
#endif // SELECTGRAPHWIDGET_H
