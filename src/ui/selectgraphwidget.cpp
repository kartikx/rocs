/*
 *  Copyright 2020  Kartik Ramesh <kartikx2000@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License or (at your option) version 3 or any later version
 *  accepted by the membership of KDE e.V. (or its successor approved
 *  by the membership of KDE e.V.), which shall act as a proxy
 *  defined in Section 14 of version 3 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "selectgraphwidget.h"
#include "libgraphtheory/graphdocument.h"

#include <QCheckBox>
#include <QList>
#include <QString>
#include <QDialogButtonBox>
#include <QPushButton>

SelectGraphWidget::SelectGraphWidget(Project* project, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::SelectGraphWidget)
    , m_project(project)
{

    ui->setupUi(this);

    m_graphDocuments = project->graphDocuments();

    m_buttonBox = ui->buttonBox;

    m_buttonBox->button(QDialogButtonBox::YesToAll)->setText("Select All");
    m_buttonBox->button(QDialogButtonBox::NoToAll)->setText("Deselect All");

    setUpWidget();

    connect(m_buttonBox->button(QDialogButtonBox::Ok), &QPushButton::clicked,
            this, &QDialog::accept);
    connect(m_buttonBox->button(QDialogButtonBox::Ok), &QPushButton::clicked,
            this, &SelectGraphWidget::modifyVisibleGraphDocumentList);
    connect(m_buttonBox->button(QDialogButtonBox::YesToAll), &QPushButton::clicked,
            [=]() {this->selectCheckBoxAuto(true);});
    connect(m_buttonBox->button(QDialogButtonBox::NoToAll), &QPushButton::clicked,
            [=]() {this->selectCheckBoxAuto(false);});
}

SelectGraphWidget::~SelectGraphWidget()
{
    delete ui;
}

void SelectGraphWidget::setUpWidget()
{
    for(int i = 0; i < m_graphDocuments.size(); ++i) {
        m_checkBoxList.append(new QCheckBox((m_graphDocuments.at(i))->documentName(), this));
        ui->checkBoxLayout->addWidget(m_checkBoxList.at(i));
    }
}

void SelectGraphWidget::modifyVisibleGraphDocumentList(){
    for(int i = 0; i < m_checkBoxList.size(); ++i) {
        if(m_checkBoxList.at(i)->isChecked()){
            m_graphDocuments.at(i)->setVisible(true);
        } else {
            m_graphDocuments.at(i)->setVisible(false);
        }
    }
    emit m_project->visibleGraphDocumentListChanged();
}

void SelectGraphWidget::selectCheckBoxAuto(bool check){
    for(int i = 0; i < m_checkBoxList.size(); ++i) {
        m_checkBoxList.at(i)->setChecked(check);
    }
}
